section .rodata
MSG:	DB	"welcome to sortMe, please sort me",10,0
S1:	DB	"%d",10,0 ; 10 = '\n' , 0 = '\0'
MY_LINE DB	10,"Sorted array",10,0

section .data

array	DB 5,1,7,3,4,9,12,8,10,2,6,11	; unsorted array
len	DB 12	
  
section .text
	align 16
	global main
	extern printf

main:
	push MSG	; print welcome message
	call printf
	add esp,4	; clean the stack 
	
	call printArray ;print the unsorted array

	push MY_LINE
	call printf
	add esp, 4
	
	call insertionSort
	call printArray

	mov eax, 1 	;exit system call
	int 0x80

printArray:
	push ebp	;save old frame pointer
	mov ebp,esp	;create new frame on stack
	pusha		;save registers

	mov eax,0
	mov ebx,0
	mov edi,0

	mov esi,0	;array index
	mov bl,byte[len]
	add edi,ebx	; edi = array size

print_loop:
	cmp esi,edi
	je print_end
	mov al ,byte[array+esi]	;set num to print in eax
	push eax
	push S1
	call printf
	add esp,8	;clean the stack
	inc esi
	jmp  print_loop

print_end:
	popa		;restore registers
	mov esp,ebp	;clean the stack frame
	pop ebp		;return to old stack frame
	ret

insertionSort:
	push ebp
	mov ebp, esp
	pusha
	
	mov esi, 1	; out loop counter
	xor edx, edx	; edx = 0
	mov dl, byte[len]	; edx = array size
	sort:
		mov edi, esi	; in loop counter
		nested_loop:
			cmp edi, 0
			jle end_nested_loop
			xor ebx, ebx	; ebx = 0
			mov bl, byte[array+edi]
			cmp bl, byte[array+edi-1]
			jge end_nested_loop
			cmp esi, edx
			jge end_sort
			xor eax, eax	; eax = 0
			xor ebx, ebx	; ebx = 0
			mov al, byte[array+edi]
			mov bl, byte[array+edi-1]
			mov byte[array+edi], bl
			mov byte[array+edi-1], al
			dec edi
			jmp nested_loop
		end_nested_loop:
			inc esi
			jmp sort
	end_sort:
		popa
		mov esp, ebp
		pop ebp
	ret
			
				
