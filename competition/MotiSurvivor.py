# !/usr/bin/python

import signal
import os

LEN = 1024

def ignoreSignal(sig, frame):
    pass

while(os.access("/tmp/GO", os.F_OK)):
    pass

signal.signal(signal.SIGINT, ignoreSignal)
signal.signal(signal.SIGQUIT, ignoreSignal)
signal.signal(signal.SIGTERM, ignoreSignal)

fileName = os.path.basename(__file__)

files = os.listdir(os.getcwd())

for row in files:
    if row != fileName and row != ".idea":
        os.system("pidof " + row + " | xargs kill -SIGKILL")
