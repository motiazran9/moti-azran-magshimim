#include <unistd.h>
#include <sys/wait.h> 
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LineParser.h"

#define PATH_MAX 2048
#define HISTORY_MAX 15
#define COMMAND_NAME 0
#define CHILD_ID 0
#define EXEC_ERROR -1
#define TRUE 1
#define FALSE 0
#define PATH 1

typedef struct LinkedList_s
{
	cmdLine *line;
	struct LinkedList_s *next;
}LinkedList;

typedef struct myQueue_s
{
	int maxSize;
	int count;
	LinkedList *head;
}Queue;

void runShell();
void echo(char * const args[], int argc);
void changeDir(char path[]);
int execute(cmdLine *pcmdLine, Queue q);
void init(Queue *q, int maxSize);
void removeFirstElement(Queue *q);
void addElement(Queue *q, char str[]);
void addNode(LinkedList **head, char str[]);
void printList(LinkedList *head);
void freeList(LinkedList **head);
void runHistoryCommand(Queue q, int place);

int main()
{
	runShell();

	return 0;
}

void runShell()
{
	char cwd[PATH_MAX], command[PATH_MAX], check = TRUE;
	cmdLine *line = NULL;
	Queue q;
	init(&q, HISTORY_MAX);

	while(check == TRUE)
	{
		getcwd(cwd, PATH_MAX);

		do{
			printf("\n%s>> ", cwd);

			fgets(command, PATH_MAX, stdin);
		}while(strncmp(command, "\n", 1) == 0);

		line = parseCmdLines(command);
		addElement(&q, command);

		check = execute(line, q);
		freeCmdLines(line);
	}

	freeList(&(q.head));
}

int execute(cmdLine *pcmdLine, Queue q)
{
	int cpid, tpid, status;

	if(strncmp(pcmdLine->arguments[COMMAND_NAME], "quit", 4) == 0)
	{
		printf("Bye Bye!\n");
		return FALSE;
	}
	else if(strncmp(pcmdLine->arguments[COMMAND_NAME], "cd", 2) == 0)
	{
		changeDir(pcmdLine->arguments[PATH]);
	}
	else if(strncmp(pcmdLine->arguments[COMMAND_NAME], "myecho", 6) == 0)
	{
		echo(pcmdLine->arguments, pcmdLine->argCount);
	}
	else if(strncmp(pcmdLine->arguments[COMMAND_NAME], "history", 2) == 0)
	{
		printList(q.head);
	}
	else if(pcmdLine->arguments[COMMAND_NAME][0] == '!')
	{
		runHistoryCommand(q, atoi(pcmdLine->arguments[COMMAND_NAME] + 1));
	}
	else
	{
		cpid = fork();
		if(fork < 0)
		{
			freeList(&(q.head));
			perror("fork");
			exit(0);
			return FALSE;
		}
		else
		{
			if(cpid == CHILD_ID)
			{
				if(execvp(pcmdLine->arguments[COMMAND_NAME], pcmdLine->arguments) == EXEC_ERROR)
				{
					perror("ERROR");
				}
				exit(1);
			}
			else
			{
				if(pcmdLine->blocking == 1)
				{
					do 
					{
						tpid = wait(&status);
				    }while(tpid != cpid);
				}
			}
		}
	}

	return TRUE;
}

void changeDir(char path[])
{
	if(path[strlen(path) - 1] == '\n')
	{
		path[strlen(path) - 1] = '\0';	
	}

	if(chdir(path) != 0)
	{
		perror("ERROR");
	}
}

void echo(char * const args[], int argc)
{
	int i;

	for(i = 1; i < argc; i++)
	{
		printf("%s ", args[i]);
	}

	printf("\n");
}

void init(Queue *q, int maxSize)
{
	q->maxSize = maxSize;
	q->count = 0;
	q->head = NULL;
}

void removeFirstElement(Queue *q)
{
	LinkedList *tmp = NULL;

	if(q == NULL || q->head == NULL)
	{
		printf("ERROR\n");
		return;
	}

	tmp = q->head;
	q->head = q->head->next;
	free(tmp);
	--q->count;
}

void addElement(Queue *q, char str[])
{
	if(q == NULL)
	{
		printf("ERROR\n");
		return;
	}

	if(q->count == q->maxSize)
	{
		removeFirstElement(q);
	}

	addNode(&(q->head), str);
	++q->count;
}

void addNode(LinkedList **head, char str[])
{
	LinkedList *tmp = NULL;

	if(head == NULL)
	{
		return;
	}

	if(*head == NULL)
	{
		tmp = ((LinkedList*)malloc(sizeof(LinkedList)));
		if(!tmp)
		{
			return;
		}

		tmp->line = parseCmdLines(str);
		tmp->next = NULL;
		*head = tmp;
		return;
	}

	addNode(&((*head)->next), str);
}

void printList(LinkedList *head)
{
	int i;

	if(head == NULL)
	{
		return;
	}

	for(i = 0; i < head->line->argCount; i++)
	{
		printf("%s ", head->line->arguments[i]);
	}

	printf("\n");
	printList(head->next);
}

void freeList(LinkedList **head)
{
	if(head == NULL || *head == NULL)
	{
		return;
	}

	freeList(&((*head)->next));
	free(*head);

	*head = NULL;
}

void runHistoryCommand(Queue q, int place)
{
	LinkedList *currNode = q.head;
	int i;

	if(place > q.count || place <= 0)
	{
		printf("ERROR: invalid number\nEnter a number between(1 - %d)\n", q.count);
		return;
	}
	else if(q.head == NULL)
	{
		printf("ERROR: the history is empty\n");
		return;
	}

	for(i = 1; i < place; i++)
	{
		currNode = currNode->next;
	}

	execute(currNode->line, q);
}