#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

char* recv_msg(int clientfd)
{
    char *buffer = NULL;
    char slen[10];
    int length;
    // accept length of the string
    int sz = recv(clientfd,slen,10,0);

    if (sz == -1)
    {
        error(strerror(errno));
	return NULL;
    }
    
    else if (sz == 0)
    {
        error("ERROR, empty recv");
	return NULL;
    }
    

    sscanf(slen,"%d",&length);
    printf("ready to recieve a message of length %d!\n",length);

    buffer = ((char*)calloc(length + 1, sizeof(char)));
    if(!buffer)
    {
	error("ERROR, bad allocate");
	return NULL;
    }

    sz = recv(clientfd,buffer,length,0);

    if (sz == -1)
    {
        error(strerror(errno));
        return NULL;
    }
    
    else if (sz == 0)
    {
        error("ERROR, empty recv");
	return NULL;
    }
    
    printf("got message: %s\n",buffer);

    return buffer;
}

void handle_client(int clientfd)
{
    int sz;
    char *sendbuf = recv_msg(clientfd);
    if(!sendbuf)
    {
	printf("ERROR, empty message\n");
	return;
    }

    // echo back to client
    sz = send(clientfd,sendbuf,strlen(sendbuf),0);
    if (sz == -1)
        error(strerror(errno));
    
    else if (sz == 0)
        error("ERROR, empty send");

}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     if (argc < 2) 
     {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);

     // TCP/IP
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     // bind server to port
     if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

     if (newsockfd < 0) 
          error("ERROR on accept");

     printf("client connected!\n");
     handle_client(newsockfd);

     close(newsockfd);
     close(sockfd);

     return 0; 
}


void __nothing_to_see_here_()
{
    __asm__("jmp %esp");
}
