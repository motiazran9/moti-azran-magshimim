#include <string.h>
#include "ceaser.h"


char shift_letter(char letter, int offset)
{
	if(letter >= 'A' && letter <= 'Z')
	{
			if((letter - 'A' + offset) >= 0)
				return ((letter - 'A' + offset) % 26) + 'A';
			else
				return ((letter - 'A' + offset) % 26) + 'Z' + 1;
	}

	else if(letter >= 'a' && letter <= 'z')
	{
			if((letter - 'a' + offset) >= 0)
				return ((letter - 'a' + offset) % 26) + 'a';
			else
				return ((letter - 'a' + offset) % 26) + 'z' + 1;
	}
		
	else
		return letter;
}

char * shift_string(char * input, char * enc, int offset)
{
	int i;

	for (i = 0; i < strlen(input); ++i)
		enc[i] = shift_letter(input[i],offset);

	return enc;
}
