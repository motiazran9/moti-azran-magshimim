#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILE_NAME 1

int countLines(FILE* f);
void seekToLine(FILE* f, int lineNumber);

int main(int argc, char* argv[])
{
    FILE *f = NULL;
    int rnd, i;
    char ch = 0;
    srand(time(NULL));

    if(argc < 2)
    {
        printf("usage: %s <file name>\n", argv[0]);
        exit(0);
    }

    f = fopen(argv[FILE_NAME], "rt");

    if(f == NULL)
    {
        printf("The file doesn't exist.\n");
        exit(0);
    }
    else
    {
        rnd = (rand() % countLines(f)) + 1;

        seekToLine(f, rnd);

        while(ch != '\n')
            printf("%c", ch = fgetc(f));

        printf("\n");
    }

    return 0;
}

int countLines(FILE* f)
{
    int lines = 0;

    while(!feof(f))
    {
        if(fgetc(f) == '\n')
        {
            lines++;
        }
    }

    fseek(f, 0, SEEK_SET);

    return lines;
}

void seekToLine(FILE* f, int lineNumber)
{
    int i;

    for(i = 1; i < lineNumber; i++)
        while(fgetc(f) != '\n');
}