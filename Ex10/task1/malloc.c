#include <unistd.h>

void* malloc(size_t size);

int main()
{
	char *str = ((char*)malloc(6));

	str = "abcd\n";

	write(0, str, 5);

	return 0;
}

void* malloc(size_t size)
{
	void *ptr = sbrk(0);

	if(sbrk(size) == ((void*)-1))
	{
		return NULL;
	}

	return ptr;
}
