#ifndef MM_H_INCLUDED
#define MM_H_INCLUDED

#include <stdio.h>

void* malloc(size_t size);
void free(void* ptr);
void* realloc(void* ptr, size_t size);

#endif // MM_H_INCLUDED

