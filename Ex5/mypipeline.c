#include <unistd.h>

#define ERROR -1
#define READER 0
#define WRITER 1
#define SIZE 2

int main()
{
	int fd[SIZE], fpid, spid, newFd, status;
	char* com1[3] = {"ls", "-l"};
	char* com2[5] = {"tail", "-n 2"};

	if(pipe(fd) == ERROR)
	{
		perror("pipe");
	}

	fpid = fork();
	if(fpid < 0)
	{
		perror("fork");
	}
	else if(fpid == 0)
	{
		close(STDOUT_FILENO);
		newFd = dup(fd[WRITER]);
		close(fd[WRITER]);
		if(execvp(com1[0], com1) == ERROR)
		{
			perror("ERROR");
		}
		close(newFd);
	}
	else
	{
		close(fd[WRITER]);
		close(newFd);

		spid = fork();
		if(spid < 0)
		{
			perror("fork");
		}
		else if(spid == 0)
		{
			close(STDIN_FILENO);
			newFd = dup(fd[READER]);
			close(fd[READER]);
			if(execvp(com2[0], com2) == ERROR)
			{
				perror("ERROR");
			}
			close(newFd);
		}
		else
		{
			waitpid(fpid, &status, 0);
			waitpid(spid, &status, 0);
			close(fd[READER]);
			close(newFd);
		}
	}

	return 0;
}
