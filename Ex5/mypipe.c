#include <unistd.h>
#include <stdio.h>

#define READER 0
#define WRITER 1
#define SIZE 2

int main()
{
	char buf[10];
	int pid, fd[SIZE];

	if(pipe(fd) == -1)
	{
		perror("pipe");
		return 1;
	}

	pid = fork();

	if(pid < 0)
	{
		perror("fork");
		return 1;
	}
	else if(pid > 0)
	{
		write(fd[WRITER], "Magshimim", 10);
	}
	else
	{
		read(fd[READER], buf, 10);
		printf("%s\n", buf);
	}

	close(fd[WRITER]);
	close(fd[READER]);

	return 0;
}